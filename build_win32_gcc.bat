@echo off
setlocal

if "%1" == "" (
	goto :USAGE
) else (
	if /I "%1%" == "x64" (
		set BUILD_ARCH=%1%
	) else (
		if /I "%1%" == "x86" (
			set BUILD_ARCH=%1%
		) else (
			goto :USAGE
		)
	)
)

if "%2%" == "" (
	goto :USAGE
) else (
	if /I "%2%" == "release" (
		set BUILD_MODE=%2%
	) else (
		if "%2%" == "debug" (
			set BUILD_MODE=%2%
		) else (
			goto :USAGE
		)
	)
)

if exist %CommonProgramFiles(x86)% (
	set WINDOWS="WIN64"
) else (
	set WINDOWS="WIN32"
)


set ROOT=%cd%
set INCLUDE_PATHS=-I%ROOT% -I%ROOT%\include
set LIBRARY_PATHS=
set DEFINES=-D_CRT_SECURE_NO_WARNINGS

REM -pedantic 
if "%BUILD_MODE%" == "debug" (
	set CFLAGS=%INCLUDE_PATHS% -g -O0 -Wall -Wextra -Wno-missing-field-initializers -Wno-missing-braces -Wno-unused-parameter
	set LFLAGS=
)

if "%BUILD_MODE%" == "release" (
	set CFLAGS=%INCLUDE_PATHS% -O3 -Wall -Wextra -Wno-missing-field-initializers -Wno-missing-braces -Wno-unused-parameter
	set LFLAGS=
)

set WIN32_LIBS=-lopengl32 -lkernel32 -luser32 -lgdi32 -lwinspool -lcomdlg32 -ladvapi32 -lshell32 -lole32 -loleaut32 -luuid -lodbc32 -lodbccp32 -lws2_32 -lcrypt32 -lsetupapi -limm32 -lwinmm -lversion -ld2d1 -lNetapi32 -luserenv -ldwmapi -lwtsapi32 -ld3d9 -ld3d10 -ld3d11 -ld3d12
set LIBS= 
set SOURCES=%ROOT%/*.c

if exist LLVM\%BUILD_MODE%\%BUILD_ARCH% (
	rmdir /S /Q %BUILD_MODE%\%BUILD_ARCH%
	mkdir llvm\%BUILD_MODE%\%BUILD_ARCH%
	pushd llvm\%BUILD_MODE%\%BUILD_ARCH%
	gcc %DEFINES% %CFLAGS% %SOURCES% %LFLAGS% -o output.exe %WIN32_LIBS% %LIBS%
	popd llvm\%BUILD_MODE%\%BUILD_ARCH%
) else (
	mkdir llvm\%BUILD_MODE%\%BUILD_ARCH%
	pushd llvm\%BUILD_MODE%\%BUILD_ARCH%
	gcc %DEFINES% %CFLAGS% %SOURCES% %LFLAGS% -o output.exe %WIN32_LIBS% %LIBS%
	popd llvm\%BUILD_MODE%\%BUILD_ARCH%
)

goto :END



:USAGE
	echo "Usage: %0% x64|x86 debug|release"

:END
	endlocal
