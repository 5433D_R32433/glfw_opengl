@echo off
setlocal

if "%1" == "" (
	goto :USAGE
) else (
	if /I "%1%" == "x64" (
		set BUILD_ARCH=%1%
	) else (
		if /I "%1%" == "x86" (
			set BUILD_ARCH=%1%
		) else (
			goto :USAGE
		)
	)
)

if "%2%" == "" (
	goto :USAGE
) else (
	if /I "%2%" == "release" (
		set BUILD_MODE=%2%
	) else (
		if "%2%" == "debug" (
			set BUILD_MODE=%2%
		) else (
			goto :USAGE
		)
	)
)

if exist %CommonProgramFiles(x86)% (
	set WINDOWS="WIN64"
) else (
	set WINDOWS="WIN32"
)

set ROOT=%cd%
set INCLUDE_PATHS= -I%ROOT%
set LIBRARY_PATHS=
set DEFINES= 

if "%BUILD_MODE%" == "debug" (
	set CFLAGS=-Od -MTd -Z7 -GS- -Oi -Gm- -GR- -sdl- -FC %INCLUDE_PATHS%
	set LFLAGS=-link -machine:%BUILD_ARCH% -heap:134217728,67108864 -stack:33554432,16777216 -SUBSYSTEM:CONSOLE -NXCOMPAT:NO -DYNAMICBASE:NO -opt:ref -opt:icf
)

if "%BUILD_MODE%" == "release" (
	set CFLAGS=-O2 -MT -GS- -Oi -Gm- -GR- -sdl- -FC %INCLUDE_PATHS%
	set LFLAGS=-link -machine:%BUILD_ARCH% -heap:134217728,67108864 -stack:33554432,16777216 -SUBSYSTEM:CONSOLE -NXCOMPAT:NO -DYNAMICBASE:NO -EMITPOGOPHASEINFO -DEBUG:NONE -opt:ref -opt:icf
)

set WIN32_LIBS=opengl32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ws2_32.lib crypt32.lib setupapi.lib imm32.lib winmm.lib version.lib d2d1.lib ole32.lib Netapi32.lib userenv.lib dwmapi.lib wtsapi32.lib
set LIBS= 
set SOURCES=%ROOT%/main.c

if exist %BUILD_MODE%\%BUILD_ARCH% (
	rmdir /S /Q %BUILD_MODE%\%BUILD_ARCH%
	mkdir %BUILD_MODE%\%BUILD_ARCH%
	pushd %BUILD_MODE%\%BUILD_ARCH%
	tcc %DEFINES% %CFLAGS% %SOURCES% %LFLAGS%  -out:output.exe %WIN32_LIBS% %LIBS%
	popd %BUILD_MODE%\%BUILD_ARCH%
) else (
	mkdir %BUILD_MODE%\%BUILD_ARCH%
	pushd %BUILD_MODE%\%BUILD_ARCH%
	cl %DEFINES% %CFLAGS% %SOURCES% %LFLAGS%  -out:output.exe %WIN32_LIBS% %LIBS%
	popd %BUILD_MODE%\%BUILD_ARCH%
)

goto :END



:USAGE
	echo "Usage: %0% x64|x86 debug|release"

:END
	endlocal
